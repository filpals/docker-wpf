FROM mcr.microsoft.com/dotnet/framework/sdk:3.5-windowsservercore-ltsc2019

# Set PowerShell as default shell
SHELL ["powershell", "-Command", "$ErrorActionPreference='Stop';$ProgressPreference='SilentlyContinue';"]

# Install chocolatey
RUN Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

# Install extra development tools
RUN choco install -y git
RUN choco install -y msbuild.communitytasks
RUN choco install -y wixtoolset --version 3.11.2

# Copy all files at current location into container
WORKDIR /
COPY . .