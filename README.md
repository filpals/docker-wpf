# Microsoft WPF Docker
This is a (Window) docker container to build WPF application using MSBuild and necessary tools installed (i.e. nuget etc).

## Getting started
- Remember switch to Windows container before build the image.
- Increase GitLab Build Timeout to 2h (if apply).

```bash
$ docker build -t wpf .
$ docker run -it wpf
```

## How to use
Include this `.gitlab-ci.yml` in your project root.

```yml
stages:
  - build

wpf:
 stage: build
 image: registry.gitlab.com/filpals/docker-wpf:latest
 script:
 - cd wpf
 - dotnet build --configuration=Release wpf.csproj
 artifacts:
  paths:
  - wpf/bin/Release/netcoreapp3.0
```

## Referencs
1. https://docs.gitlab.com/runner/executors/docker.html#supported-windows-versions
2. https://gitlab.com/jelleverheyen/cd-example
3. https://stackoverflow.com/a/65392574/707752
4. https://docs.microsoft.com/en-us/visualstudio/install/build-tools-container?view=vs-2019
5. https://docs.microsoft.com/en-us/visualstudio/install/workload-component-id-vs-build-tools?view=vs-2019